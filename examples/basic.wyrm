;; 
;; Wyrm is is first and foremost a functional stack-based
;; language. It's primary model of programming is the
;; application of functions over a stack of values.
;;
;; This file focuses on the very basics of defining words
;; and accessing dependencies.
;;
vocabulary notes-00-basics defines


;;
;; Supporting libraries are always listed after the vocabulary
;; declaration. Importing vocabs cannot trigger side effects.
;; 
;; There are other features that can facilitate global state and
;; configurations, but that's outside the scope of basics.
;;
imports core strings math ;


;;
;; Words
;; Words are the basic building block of Wyrm. In other languages
;; they are referred to as Functions, but Wyrm follows terms 
;; used by Forth and Factor.
;;
;; The basic syntax of a word is a name and body. Wyrm will
;; infer the type via the provided definition. 
;;
:> greet == "Hello, " prepend "!" append ;


;;
;; Of course some Words will work on multiple types. To 
;; limit the openness of type checking, you can provide a
;; Type Declaration
;;
:> palindrome => ( String -- Bool ) == 
    dup reverse = ;

;;
;; Forward Declaration
;; Words can be declared ahead of time. This allows for 
;; mutual recursion. Definitions must be in same file.
;;
:> even? => ( UInt32 -- Bool )
:> odd?  => ( UInt32 -- Bool )

:> even? == dup 0 = [ drop True  ] [ 1 -  odd? not ] if ;
:> odd?  == dup 1 = [ drop False ] [ 1 - even? not ] if ;


;;
;; Overloading
;; As mentioned earily, you can define the same word to 
;; operate on the different types.
;;
:> double => ( Int64 -- Int64 ) ==
    2 * ;

:> double => ( String -- String ) ==
    dup append ;