# Wyrm

A statically typed stack-based programming language.

```
;;
;; A Wyrm program to do a bit of math
;;
vocabulary Main defines

;;
;; Read two floats from the commands line and print their 
;; ratio.
;;
:> print-ratio ( -- )
    :+ ( Console )
    :- ( DivideByZero )
==
    "Enter to numbers: " print-line
        [ read-line >Float64 ] twice / !! 
            to-string print-line ;


:> main ( -- )
    :+ ( Console )
==
    print-ratio case {
        :> DivideByZero => 
            "Can't divide by zero!" print-line ;
    } ;
```

Wyrm is still early in development, but a list of things I want for the
language:
- **Stack-based:** 
        Seamlessly chain together operations and return
        multiple values with ease.

- **Functional:** 
        Use functions as values, pass them to other functions, 
        and compose them together.

- **Statically typed:** 
        Lean on types to catch stack errors, walk through operations, 
        refactor code, look up definitions, and find useful words.

- **Inferred Types:** 
        Compiler can infer the types of expression. Useful for 
        combinators or trivial expressions.

- **Highly refactorable:** 
        Extract any line of code, name it, define its types,
        and then find and replace it. Freely lift expression
        for re-usage later.

- **Tracked permissions:** 
        The type system tracks what side-effects functions need 
        to operate. However, they are unintrusive to the code 
        you write.

- **Composable Types and Implementations:**
        Combine and compose types by treating them as sets. 
        Define open interfaces using structural types.

## Inspired a lot by
- Factor 
- Kitten

### Influenced a bit by
- TypeScript
- Idris